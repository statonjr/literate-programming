#+TITLE: Literate Clojure Programming
#+AUTHOR: Larry Staton Jr.
#+DATE: 2016 June 3

* Getting Started

** Prerequisites
	 - [[https://www.gnu.org/software/emacs/][Emacs]]
	 - [[https://github.com/clojure-emacs/cider][CIDER]]
	 - [[https://boot-clj.com][Boot]]

	 To evaluate code, you need a REPL. We could start up CIDER using
	 =M-x cider-jack-in=, but that will start CIDER with =leiningen=. We
	 prefer [[https://boot-clj.com][boot]].

	 In a terminal, execute =boot cider repl wait= [fn:1]. This will start a
	 =nREPL= server to which we can connect. Take note of the port and
	 host for the connection.

	 Back in Emacs, execute =M-x cider-connect= and use the port and
	 host to connect. Once you're connected, you can begin [[*Writing code][writing code]].

* Writing code

Create a code block and then execute =C-c C-c=

#+NAME: sample
#+BEGIN_SRC clojure :exports none
  (+ 1 2 3)
#+END_SRC

#+RESULTS: sample
: 6

#+BEGIN_EXAMPLE
#+NAME: sample
#+BEGIN_SRC clojure
  (+ 2 3)
#+END_SRC

#+RESULTS: sample
: 5
#+END_EXAMPLE

By default, results show in your document in a =#+RESULTS:= block. To
keep the results out of your document and to only show the value in
the mode-line, use =:results silent=.

#+NAME: no-results
#+BEGIN_SRC clojure :results silent :exports none
  (* 3 4)
#+END_SRC

#+BEGIN_EXAMPLE
#+NAME: no-results
#+BEGIN_SRC clojure :results silent
  (* 2 4)
#+END_SRC
#+END_EXAMPLE

* Exporting

In Org-mode, you can export your code to \LaTeX{} or HTML or plain text
or any various formats. The =:exports= header tells Org-mode what to
export for each source code block. The default is =code=, which
exports the body of the source code block. Another option is
=results=, which exports the result of evaluating the code. =both= and
=none= are the other options.

To export your document, execute =C-c C-e= and then choose from the
list of options. If you have \LaTeX{} installed, execute =C-c C-e l o=
and Org-mode will typeset this document, convert it to PDF, and open
it.

** What's with the =#+BEGIN_EXAMPLE...#+END_EXAMPLE= blocks?

	 Org-mode provides [[http://orgmode.org/manual/Literal-examples.html#Literal-examples][literal blocks]] that are not subject to
	 markup. The text within these blocks are printed verbatim when
	 exported. I wanted to export the code samples and need them to be
	 printed verbatim and not executed.

* Tangling

Tangling is the process of extracting source code from a plain text
file and creating a source code file.

To tangle your source code to a file, execute =C-c C-v t=

#+NAME: tangle-sample
#+BEGIN_SRC clojure :tangle yes :exports none
  (ns tangle-sample.core
    (:require [clojure.test :refer :all]))

  (defn hi [] "hi")
#+END_SRC

#+BEGIN_EXAMPLE
#+NAME: tangle-sample
#+BEGIN_SRC clojure :tangle yes
  (ns tangle-sample.core
    (:require [clojure.test :refer :all]))

  (defn hi [] "hi")
#+END_SRC
#+END_EXAMPLE

You should now see a [[file:literate-clojure-programming.clj][clojure.clj]] file in the
current directory. Note that you only see the code marked with a
=:tangle yes= header.

You can include text between the source code blocks and org-mode will
still include each block in the same file.

#+NAME: defn-greeting
#+BEGIN_SRC clojure :tangle yes :exports none
  (defn greeting
    "greet NAME"
    [name]
    (str "Greetings, " name "!"))
#+END_SRC

#+BEGIN_EXAMPLE
#+NAME: defn-greeting
#+BEGIN_SRC clojure :tangle yes
  (defn greeting
    "greet NAME"
    [name]
    (str "Greetings, " name "!"))
#+END_SRC
#+END_EXAMPLE

Execute =C-c C-v t= and you'll see both function definitions in your file.

This is great for literate programming! You can write prose directly
in the file and then =tangle= the file to extract the source code.

* Tests

** Unit tests

	 Let's add some unit tests right here.

#+NAME: greeting-test
#+BEGIN_SRC clojure :results output append :tangle yes :exports none
  (deftest test-greeting
    (testing "Name is correct"
      (is (= "Greetings, Tux!" (greeting "Tux")))))

  (run-tests)
#+END_SRC

#+RESULTS: greeting-test
:
: Testing tangle-sample.core
:
: Ran 1 tests containing 1 assertions.
: 0 failures, 0 errors.

#+BEGIN_EXAMPLE
#+NAME: greeting-test
#+BEGIN_SRC clojure :results output append :tangle yes
  (deftest test-greeting
    (testing "Name is correct"
      (is (= "Greetings, Tux!" (greeting "Tux")))))

  (run-tests)
#+END_SRC

#+RESULTS: greeting-test
:
: Testing tangle-sample.core
:
: Ran 1 tests containing 1 assertions.
: 0 failures, 0 errors.
#+END_EXAMPLE

Execute =C-c C-c= in the above code block and you should see a passing
test!

** test.check

** clojure.spec
* ClojureScript

I wonder if it works with ClojureScript.

#+NAME: defn-cljs
#+BEGIN_SRC clojurescript :exports none
  (+ 2 3)
#+END_SRC

#+BEGIN_EXAMPLE
#+NAME: defn-cljs
#+BEGIN_SRC clojurescript
  (+ 2 3)
#+END_SRC
#+END_EXAMPLE

Doesn't execute, but you could tangle the code to a =.cljc= file.

You can give the =:tangle= header a relative path and Org-mode will
write the source code to that path.

Let's try that.

#+NAME: cljc-test
#+BEGIN_SRC clojurescript :tangle test.cljc :exports none
  (defn hello [] "hello")
#+END_SRC

#+BEGIN_EXAMPLE
#+NAME: cljc-test
#+BEGIN_SRC clojurescript :tangle test.cljc
  (defn hello [] "hello")
#+END_SRC
#+END_EXAMPLE

Interesting! Org-mode output this code block to [[file:test.cljc][test.cljc]] and the
other code blocks to [[file:literate-clojure-programming.clj][clojure.clj]].

What happens if we add another function?

#+NAME: another-function
#+BEGIN_SRC clojure :tangle yes :exports none
  (defn hello [] "hello")
#+END_SRC

#+BEGIN_EXAMPLE
#+NAME: another-function
#+BEGIN_SRC clojure :tangle yes
  (defn hello [] "hello")
#+END_SRC
#+END_EXAMPLE

Org-mode puts =another-function= in [[file:literate-clojure-programming.clj][clojure.clj]]!

* Tips and Tricks

The [[https://github.com/thi-ng][thi.ng project]] uses literate programming with Org files
extensively.

If you see the following error:
=org-babel-tangle-single-block: Wrong type argument: stringp, nil=
then you probably have bad markup in a source code block.

To =tangle= the code at point, use a prefix =C-u C-c C-v t=

[fn:1] You'll need the [[https://github.com/boot-clj/boot/wiki/Cider-REPL#a-better-way][cider]] task for boot.
